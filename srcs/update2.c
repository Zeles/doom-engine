/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 19:41:03 by gdaniel           #+#    #+#             */
/*   Updated: 2019/06/12 17:22:24 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "doom.h"

void	checktype(t_doom *doom, size_t secid, double delta)
{
	if (doom->thismap.sectors[secid].type == 2 && !doom->player.poison_defense)
		minushealth(&doom->player, (0.1 * doom->player.damagemult) * delta);
}
